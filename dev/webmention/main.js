import fetch, { MOCK_NOTE } from './fetch.js';
import Webmention from './webmention.js';

export { fetch };

export const Ping = (origin, endpoint = '/webmention') => Webmention(origin, MOCK_NOTE, endpoint);
