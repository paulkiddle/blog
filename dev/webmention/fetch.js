import nodeFetch from 'node-fetch';
import fs from 'fs';

const note = new URL('./note.html', import.meta.url).pathname;

async function Res(status, body=''){
	return {
		status,
		ok: status < 300,
		async text(){
			return body;
		}
	}
}

function getNote() {
	if(fs.existsSync(note)) {
		return Res(200, fs.readFileSync(note));
	} else {
		return Res(404);
	}
}

export const MOCK_NOTE = 'http://mock.example/'

export default function fetch(url, ...args) {
	if(String(url) === MOCK_NOTE) {
		return getNote()
	} else {
		return nodeFetch(url, ...args);
	}
}
