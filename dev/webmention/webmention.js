import fetch from 'node-fetch';

export default (origin, source, endpoint) => async function(url){
	const target = new URL(url, origin);

	endpoint = new URL(endpoint, origin);

	return fetch(
		endpoint,
		{
			headers: {
				'content-type': 'application/x-www-form-urlencoded'
			},
			method: 'POST',
			body: new URLSearchParams({ source, target }).toString() });
}
