import Worker from './webmentions/worker.js';
import Posts from './posts/index.js';
import GetUser from './ui/users/index.js';


export default async function QueueRouter({ sql, notify, fetch }) {
	const posts = await Posts(sql);
	const getUser = GetUser(posts, notify);
	const webmentions = Worker(getUser, fetch);

	return async function queueRouter(message) {
		if(message.type === 'webmention') {
			await	webmentions(message.source, message.target);
			return;
		}
	};
}