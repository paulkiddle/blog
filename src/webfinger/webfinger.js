/**
 * An error resulting from parsing the webfinger request
 */
//  export class WebfingerResourceError extends TypeError {
// 	get status() { return 400; }
// }

export function formatResult(result, subject) {
	if(!result) {
		return formatResult([]);
	}

	// if(result.links) {
	// 	return result;
	// }

	if(Array.isArray(result)) {
		return { subject, links: result };
	}

	// if(typeof result === 'string') {
	return {
		subject,
		links: [
			{
				rel: 'self',
				type: 'application/activity+json',
				href: result
			}
		]
	};
	// }

	// return {
	// 	subject,
	// 	links: [result]
	// };
}

export class AccountResource {
	#parts

	constructor(username, host) {
		this.#parts = [username, host];
	}

	get username(){
		return this.#parts[0];
	}

	get host(){
		return this.#parts[1];
	}

	toString(){
		return this.username + '@' + this.host;
	}
}

/**
 * Lookup the user and return their ID or resource object
 * @callback FindResource
 * @async
 * @param {AccountResource} resource The username & host of the resource being looked up
 * @return {Object|String|Array} Either the entire webfinger response object, the array of links objects, or the user's href
 */

/**
 * Return middleware for handling a webfinger lookup
 * @param {FindResource} findResource The function that looks up and returns the user's ID
 * @returns {Function} Webfinger middleware
 */
export default function webfinger(findResource){
	const matcher = /^acct:([^@]+)@(.+)$/i;

	return async (req, res) =>	{
		const url = new URL(req.url, 'file://');

		if(url.pathname !== '/.well-known/webfinger') {
			return false;
		}

		if(req.method !== 'GET') {
			res.status = 405;
			res.end('This endpoint only accepts GET requests');
			return true;
		}

		const resource = url.searchParams.get('resource');

		const match = resource && resource.match(matcher);

		if(!match) {
			//throw new WebfingerResourceError('Webfinger expected a resource of the form "acct:username@domain.example"');
			res.status = 400;
			res.end('Webfinger expected a resource of the form "acct:username@domain.example"');
			return true;
		}

		const [, username, host] = match;

		const result = formatResult(await findResource(new AccountResource(username, host)), resource);

		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Content-Type', 'application/jrd+json');
		res.end(JSON.stringify(result));
		return true;
	};
}
