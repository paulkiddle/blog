import { jest } from '@jest/globals';
import Webfinger, { AccountResource, formatResult } from './webfinger.js';

class Req {
	constructor(user, method = 'GET') {
		this.url = '/.well-known/webfinger?resource=acct:' + user
		this.method = method;
	}
}

class Res {
	end=jest.fn()
	setHeader=jest.fn()
}

test('webfinger', async()=>{
	const lookup = jest.fn()
	const webfinger = Webfinger(lookup);

	const req = new Req('e@b.com');
	
	const res = new Res;

	expect(await webfinger(req, res)).toBe(true);

	expect(lookup).toHaveBeenCalledWith(expect.any(AccountResource));
	expect(lookup.mock.calls[0][0].toString()).toEqual('e@b.com');
	expect(res.end.mock.calls).toMatchSnapshot();
});

test('webfinger invalid domain', async ()=>{
	const lookup = jest.fn()
	const webfinger = Webfinger(lookup);

	const req = new Req('fake');
	const res = new Res;

	expect(await webfinger(req, res)).toBe(true);

	expect(res.end.mock.calls).toMatchSnapshot();
})


test('webfinger invalid method', async ()=>{
	const lookup = jest.fn()
	const webfinger = Webfinger(lookup);

	const req = new Req('a@b.com', 'POST');
	const res = new Res;

	expect(await webfinger(req, res)).toBe(true);

	expect(res.end.mock.calls).toMatchSnapshot();
})


test('webfinger invalid url', async ()=>{
	const lookup = jest.fn()
	const webfinger = Webfinger(lookup);

	const req = {
		url: '/other'
	}
	const res = new Res;

	expect(await webfinger(req, res)).toBe(false);

	expect(res.end).not.toHaveBeenCalled();
})

test('format result from string', ()=>{
	expect(formatResult('string')).toMatchSnapshot()
})