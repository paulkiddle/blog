import Post from '../ui/users/post.js';
import Blog from '../ui/routes/home/blog.js';
import Inbox from '../ui/routes/inbox/inbox.js';
import { menu } from '../ui/routes/index.js';
import contentType from 'content-type';
import WebmentionDeserializer from '../deserializers/webmention.js';
import FormDeserializer from '../deserializers/form-urlencoded.js';
import ActivityDeserializer from '../deserializers/activity-json.js';
import accepts from 'accepts';
import HtmlSerializer from '../serlializers/html.js';
import JsonSerializer, { jsonld } from '../serlializers/json.js';


const activityJson = 'application/activity+json';

export const paths = {
	home: '/',
	inbox: '/$inbox',
	feed: menu.feed
};

export default class User {
	#posts
	#keypair
	#enqueue

	constructor({ posts, keypair, enqueue }) {
		this.#posts = posts;
		this.#keypair = keypair;
		this.#enqueue = enqueue;
	}

	async getResource(uri) {
		switch(uri.pathname) {
		case paths.home:
			return new Blog(
				await this.getPosts(),
				{
					uri,
					username: 'Paul',
					keypair: this.#keypair,
					inbox: new URL(paths.inbox, uri).href,
					feedUrl: paths.feed
				}
			);
		case paths.inbox:
			return new Inbox({
				enqueue: this.#enqueue
			});
		}

		return null;
	}

	async getPosts(){
		return (
			await this.#posts.list()
		).map(p=>new Post(this, p));
	}

	#getDeserializer(req) {
		const { type } = contentType.parse(req.getHeader('content-type'));

		switch(type) {
		case 'application/x-www-form-urlencoded':
			return req.url === paths.inbox ? new WebmentionDeserializer(async url=>!!await this.getResource(url)) : new FormDeserializer;
		case 'application/activity+json':
		case 'application/json':
		case 'application/ld+json':
			return new ActivityDeserializer;
		}
	}

	async getBody(req) {
		const deserializer = this.#getDeserializer(req);

		return deserializer.deserialize(req);
	}

	getSerializer(request, res, renderTemplate) {
		const accept = accepts(request);

		switch(accept.type('text/html', 'application/json', activityJson, jsonld)) {
		case 'text/html':
			return new HtmlSerializer(res, renderTemplate);
		case 'application/json':
		case jsonld:
		case activityJson:
			return new JsonSerializer(res);
		}

		throw new TypeError('Not acceptable');
	}
}