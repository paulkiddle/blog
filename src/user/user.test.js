import HtmlSerializer from "../serlializers/html.js";
import JsonSerializer from "../serlializers/json.js";
import User, { paths } from "./user"

test('user get resource', async()=>{
	const user = new User({ posts: {
		list(){
			return []
		}
	} });

	expect(await user.getResource(new URL('file:///'))).toMatchSnapshot();
	expect(await user.getResource(new URL('file:///404'))).toBe(null);
	expect(await user.getResource(new URL(paths.inbox, 'file://'))).toMatchSnapshot();
})

test('user get posts', async()=>{
	const postStore = {
		list(){
			return [{}]
		}
	};
	const user = new User({ posts: postStore });

	expect(await user.getPosts()).toMatchSnapshot()
})

test('get req body', async()=>{
	const posts = {
		list(){
			return [{}]
		}
	};
	const req = {
		getHeader(){
			return this.type;
		},
		url: '/',
		body: 'a=b',
		type: 'application/x-www-form-urlencoded',
		async * [Symbol.asyncIterator](){
			yield this.body
		}
	}

	const user = new User({ posts });
	expect(await user.getBody(req)).toMatchSnapshot();

	req.url = '/$inbox';
	req.body = 'source=http://a.example&target=http://b.example'

	expect(await user.getBody(req)).toMatchSnapshot();

	req.type = 'application/json';
	req.body = '{}';

	expect(await user.getBody(req)).toMatchSnapshot();
})

test('get serializer', async()=>{
	const req = {
		headers: {
			accept: 'text/html'
		}
	}

	const user = new User({});

	expect(await user.getSerializer(req)).toBeInstanceOf(HtmlSerializer);

	req.headers.accept = 'application/json';

	expect(await user.getSerializer(req)).toBeInstanceOf(JsonSerializer);
})