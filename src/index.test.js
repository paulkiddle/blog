import createApp from './index.js';
import Sql from 'knex-sqlite';

test('Creates app handler', async ()=>{
	const sql = Sql(':memory:');
	expect(await createApp({ sql, jwtSecret: 'secret', origin: 'http://example.com' })).toBeInstanceOf(Function);
});

test('Worker creates iterator', async()=>{
	const sql = Sql(':memory:');
	expect(await createApp.worker({ sql })).toMatchObject({ [Symbol.asyncIterator]: expect.any(Function) });
});
