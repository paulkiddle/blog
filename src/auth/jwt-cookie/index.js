import cookie from 'cookie';
import jwt from 'jsonwebtoken';

class JwtCookie {
	#secret;
	#cookieKey;

	constructor(secret, cookieKey='session'){
		this.#secret = secret;
		this.#cookieKey = cookieKey;
	}

	get(req) {
		const c = req.headers.cookie;
		if(!c) {
			return c;
		}
		const cookies = cookie.parse(c);
		const token = cookies[this.#cookieKey];
		const decoded = token && jwt.verify(token, this.#secret);
		return decoded;
	}

	set(res, payload) {
		const token = jwt.sign(payload, this.#secret);
		const c = cookie.serialize(this.#cookieKey, token, { httpOnly: true });
		res.setHeader('Set-Cookie', c);
	}
}

export default JwtCookie;
