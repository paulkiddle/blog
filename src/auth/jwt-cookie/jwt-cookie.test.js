import JwtCookie from './index.js';
import { jest } from '@jest/globals';

test('JWTCookie', () => {
	const j = new JwtCookie('sec', 'ses');

	const res = {
		setHeader: jest.fn((n,c)=>res.cookie=c)
	}

	j.set(res, {data: 'my-data'});

	expect(res.setHeader).toHaveBeenCalledWith("Set-Cookie", expect.stringMatching(/^ses=/));

	const req = {
		headers: {
			cookie: res.cookie.split(';')[0]
		}
	}

	expect(j.get(req)).toEqual(expect.objectContaining({data:'my-data'}))
})
