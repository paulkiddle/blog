import MAuth, {migrate as authMigrate} from 'knex-masto-auth';
import JwtCookie from './jwt-cookie/index.js';

export const migrate = sql => authMigrate(sql);

export default class Auth {
	#getUser;
	#session;
	#auth;
	#exchangeEndpoint;

	constructor({ getUser, secret, clientName, origin, exchangeEndpoint = '/exchange-token', sql }){
		this.#getUser = getUser;
		this.#session = new JwtCookie(secret);
		this.#exchangeEndpoint = exchangeEndpoint;
		const clientOptions = {
			clientName,
			redirectUri: origin+exchangeEndpoint
		};

		this.#auth = new MAuth(sql, clientOptions, {});
	}

	getRedirectUrl(url) {
		return this.#auth.getRedirectUrl(url);
	}

	async handleCallback(req, res) {
		const auth = this.#auth;
		const url = new URL(req.url, 'http://' + req.headers.host);
		if(url.pathname === this.#exchangeEndpoint) {
			const ui = await auth.getUserFromCallback(req);

			const serialized = { id: ui.sub, data: { name: ui.name, avatar: ui.picture  }};

			this.#session.set(res, serialized);
			return true;
		}

		return false;
	}

	getUser(req) {
		const { id, data } = this.#session.get(req) || {};

		return this.#getUser(id, data, id && new URL(id).origin);
	}
}
