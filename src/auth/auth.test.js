import Auth, { migrate } from './auth.js';
import Sql from 'knex-sqlite';
import { jest } from '@jest/globals';

test('Auth', async () => {
	const sql = Sql(':memory:');

	await migrate(sql);
	const auth = new Auth({
		getUser: jest.fn(),
		secret: 'jwtSecret',
		clientName: 'Client',
		origin: 'http://example.com',
		sql
	});

	const req = {
		headers: {}
	}
	expect(await auth.handleCallback(req, {})).toBe(false);
})
