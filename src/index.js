import archived from './archived/index.js';
import Ui from './ui/index.js';
import Posts from './posts/index.js';
import Auth, {migrate} from './auth/auth.js';
import GetUser from './ui/users/index.js';
import Webfinger from './webfinger/webfinger.js';

export const webmentionEndpoint = '/.webmention';

async function app ({ sql, jwtSecret, origin, notify, clientName, enqueue, keypair } = {}) {
	const posts = await Posts(sql);
	const getUser = GetUser(
		posts,
		notify,
		keypair,
		enqueue
	);

	const home = await getUser.visitor.getResource(new URL('/', origin));
	const homeUser = home.username.toLowerCase();

	const webfinger = Webfinger(resource => resource.username.toLowerCase() === homeUser ? home.id : null);

	if(!jwtSecret) {
		throw new TypeError('Not JWT Secret provided');
	}

	await migrate(sql);
	const auth = new Auth({
		getUser,
		secret: jwtSecret,
		clientName,
		origin,
		sql
	});

	const ui = await Ui(
		{
			origin: origin,
			title: 'Develoraptor Blog',
			colour: '#99cc00',
			auth,
			webmentionEndpoint: origin+webmentionEndpoint
		}
	);

	async function handler (req, res) {
		if(await archived(req, res)){
			return;
		}

		if(await webfinger(req, res)) {
			return;
		}

		await ui(req, res);
	}

	return handler;
}

export default app;

import Q from './queue.js';

export async function* worker({ sql, notify, queue, fetch, log }){
	const work = await Q({sql, notify, fetch});

	for await(const message of queue) {
		try {
			await work(message);
			queue.delete();
		} catch(e) {
			log(e);
		}
		yield;
	}
}

app.worker = worker;
