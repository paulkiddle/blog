import Blog from "../ui/routes/home/blog.js";
import JsonSerializer, { UnserializableError } from "./json";

class Res {
	output = '';
	headers = {}

	write(chunk){
		this.output += chunk;
	}

	end(chunk=''){
		this.output += chunk;
	}

	setHeader(h, v){
		this.headers[h]= v;
	}
}

test('Json serializer', async()=>{
	const res = new Res;
	const jsonSerializer = new JsonSerializer(res);
	const blog = Object.setPrototypeOf({
		id: 'http://example.com',
		inbox: 'http://example.com/inbox',
		username: 'Paul',
		publicKey: 'rsa key'
	}, Blog.prototype);

	await jsonSerializer(blog);

	expect(await res.output).toMatchSnapshot();
});

test('Throws error for invalid resource', ()=>{
	const jsonSerializer = new JsonSerializer(new Res);
	expect(()=>jsonSerializer({})).toThrow(UnserializableError)
})


test('Json created serializer', async()=>{
	const res = new Res;
	const jsonSerializer = new JsonSerializer(res);

	const blog = Object.setPrototypeOf({
		id: 'http://example.com',
		inbox: 'http://example.com/inbox',
		username: 'Paul',
		publicKey: 'rsa key'
	}, Blog.prototype);


	await jsonSerializer.created(blog);

	expect(await res.headers).toMatchSnapshot();
});