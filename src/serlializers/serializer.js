function BaseSerializer(serializer) {
	return Object.setPrototypeOf(
		resource => serializer(resource),
		new.target.prototype
	);
}
BaseSerializer.prototype = Function.prototype;

export default class Serializer extends BaseSerializer {
	#res

	constructor(res, serializer) {
		super(serializer);

		this.#res = res;
	}

	created(response){
		if(response?.id) {
			this.#res.setHeader('location', response.id);
		}
		this.#res.end();
	}

	methodNotAllowed() {
		this.#res.statusCode = 405;
		this.#res.end();
	}
}