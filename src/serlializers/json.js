import Blog from '../ui/routes/home/blog.js';
import Serializer from './serializer.js';
// import asContext from './activitystreams.js';
// import secContext from './security-v1.js';
// import j from 'jsonld';
// const jsonld = j.promises;

const context = [
	'https://www.w3.org/ns/activitystreams',
	'https://w3id.org/security/v1'
];


export class UnserializableError extends TypeError {}

export const jsonld = 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"';

export default class JsonSerializer extends Serializer {
	#res
	
	constructor(res) {
		super(
			res,
			resource => {
				res.setHeader('content-type', jsonld);
				res.end(this.#serialize(resource));
			}
		);

		this.#res = res;
	}

	#serialize(resource){
		return JSON.stringify(this.#toActivityJson(resource));
	}

	created(resource){
		this.#res.setHeader('content-type', jsonld);
		this.#res.statusCode = 201;
		this.#res.write(this.#serialize(resource));
		super.created(resource);
	}

	#toActivityJson(resource){
		if(resource instanceof Blog) {
			const id = resource.id.toString();
			return {
				'@context': context,
				id,
				inbox: resource.inbox,
				outbox: resource.id+'outbox/',
				preferredUsername: resource.username,
				type: 'Person',
				name: resource.username,
				publicKey: {
					id: id + '#main-key',
					owner: id,
					publicKeyPem: resource.publicKey
				}
			};
		} else {
			throw new UnserializableError('Resource cannot be serialized as JSON');
		}
	}
}