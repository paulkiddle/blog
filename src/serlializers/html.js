import {view} from '../ui/routes/home/index.js';
import { finished } from  'stream/promises';
import { Readable } from 'stream';
import Serializer from './serializer.js';
import Blog from '../ui/routes/home/blog.js';
import { WebmentionDeserializeError } from '../deserializers/webmention.js';

export default class HtmlSerializer extends Serializer {
	#res
	#renderHtml

	constructor(res, renderTemplate) {
		super(res, resource => {
			return this.#toHtml(resource);
		});
		this.#renderHtml = renderTemplate;
		this.#res = res;
	}

	async #toHtml(resource){
		if(resource instanceof Blog) {
			const template = view(resource);
			await finished(Readable.from(this.#renderHtml(template)).pipe(this.#res));
		} else if(resource instanceof WebmentionDeserializeError) {
			const reason = resource.message;
			this.#res.statusCode = 400;
			this.#res.status = reason;
			this.#res.end(
				`This is a webmention endpoint. Webmention is a simple way to programatically notify any URL when someone links to it from another site.
				Your webmention request failed because "${reason}".
				For detailed information on how to implement Webmention, read the official spec at https://www.w3.org/TR/webmention`);
		}	else {
			this.#res.statusCode = 406;
			this.#res.end('Could not render resource.');
		}
	}
	
	created(resource) {
		this.#res.statusCode = resource?.id ? 303 : 201;
		super.created(resource);
	}
}