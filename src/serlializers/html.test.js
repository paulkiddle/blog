import Blog from "../ui/routes/home/blog.js";
import HtmlSerializer from "./html";
import stream from 'stream';
import { WebmentionDeserializeError } from "../deserializers/webmention.js";

class Res extends stream.Writable{
	output = '';
	headers = {}

	constructor(){
		super({
			write: (chunk, e, cb) => { this.output += chunk; cb() }
		})
	}

	setHeader(h, v){
		this.headers[h] = v;
	}
}

test('Html serializer', async()=>{
	const res = new Res;
	const htmlSerializer = new HtmlSerializer(
		res,
		async function*(body){
			yield 'Body: ->'
			yield* body.generate();
			yield '<-'
		}
	);
	const blog = Object.setPrototypeOf({
		id: 'http://example.com',
		inbox: 'http://example.com/inbox',
		username: 'Paul',
		publicKey: 'rsa key',
		posts: [],
		feed: '/'
	}, Blog.prototype);

	await htmlSerializer(blog);

	expect(await res.output).toMatchSnapshot();
});

test('Html serializer no route', async()=>{
	const res = new Res;
	const htmlSerializer = new HtmlSerializer(
		res,
		async function*(body){
			yield 'Body: ->'
			yield* body.generate();
			yield '<-'
		}
	);
	const blog = {}

	await htmlSerializer(blog);

	expect(await res.output).toMatchSnapshot();
});


test('Html serializer created route', async()=>{
	const res = new Res;
	const htmlSerializer = new HtmlSerializer(
		res,
		async function*(body){
			yield 'Body: ->'
			yield* body.generate();
			yield '<-'
		}
	);

	await htmlSerializer.created({ id: 'http://example.com' });

	expect([res.statusCode, res.headers]).toMatchSnapshot();
});

test('Html serializer created route no id', async()=>{
	const res = new Res;
	const htmlSerializer = new HtmlSerializer(
		res,
		{}
	);

	await htmlSerializer.created(true);

	expect([res.statusCode, res.headers]).toMatchSnapshot();
});

test('Serialize webmention error', async () => {
	const res = new Res;
	const e = new WebmentionDeserializeError('Error!');
	const s = new HtmlSerializer(
		res,
		()=>{}
	)
	await s(e);
	expect(res.output).toMatchSnapshot()
})