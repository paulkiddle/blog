import Serializer from "./serializer.js";

class Res {
	output = '';
	headers = {}

	write(chunk){
		this.output += chunk;
	}

	end(chunk=''){
		this.output += chunk;
	}

	setHeader(h, v){
		this.headers[h]= v;
	}
}

test('Serializer method not allowed', async()=>{
	const res = new Res;
	const serializer = new Serializer(res, a=>a);
	
	await serializer.methodNotAllowed();

	expect(await res.output).toMatchSnapshot();
});
