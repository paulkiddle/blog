import Parser from './parser/main.js';

export default (getUser, fetch) => {
	const parser = Parser(fetch);

	return async function worker(source, target) {
		source = new URL(source);
		target = new URL(target);
		const event = await parser(source);
		const user = await getUser(event.actor.url, event.actor, new URL(source).origin);

		if(event.method==='POST') {
			const post = await user.getPost(target.pathname);
			await post.addComment(event.data);
		} else if(event.method === 'DELETE') {
			const comment = await user.getComment(source.href);
			await comment.delete();
		// Currently not possible for other values to be returned
		// } else {
		// 	throw new TypeError('Cannot handle webmention method ' + event.method);
		}
	};
};
