import Worker from './worker.js';
import { jest } from '@jest/globals';

test('Worker delete', async ()=> {
	const comment = {
		delete: jest.fn()
	}
	const user = {
		getComment: ()=>comment
	}
	const getUser = jest.fn(()=>user);
	const fetch = jest.fn(()=>({
		status: 404
	}));
	const worker = Worker(getUser, fetch);

	await worker('http://example.com/post', 'http://local.example/post');

	expect(comment.delete).toHaveBeenCalled();
});
