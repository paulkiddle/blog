import parser, { microformats } from './microformats.js';

const parsed = properties => ({ items: [
	{
		type: ['h-entry'],
		properties
	}
] })

test('microformats parser', ()=> {
	const baseUrl = 'http://post.example/';

	expect(parser('<div>nothing</div>', baseUrl)).toBe(null);
	expect(microformats(parsed(null))).toBe(null);
	expect(microformats(parsed({}))).toMatchSnapshot();
	expect(microformats(parsed({
		'in-reply-to':[],
		'content':[],
		'published':[],
		'author':[]
	}))).toMatchSnapshot();
});
