import jsdom from 'jsdom';
import microformats from './microformats.js';
import htmlParser from './html.js';
import merge from 'lodash.merge';
import HTML from 'encode-html-template-tag';

const { JSDOM } = jsdom;

function parseDoc(document, sourceUrl){
	const h = document.querySelector('.h-entry');

	const microData = h ? microformats(h.outerHTML, sourceUrl) : {};

	const data = merge(htmlParser(document, sourceUrl), microData);

	data.content = HTML.raw(data.content);

	return data;
}

export class HttpError extends Error { }

export default fetch => async function(sourceUrl){
	const res = await fetch(sourceUrl);
	const url = new URL(sourceUrl);

	if(!res.ok) {
		if([404, 410].includes(res.status)) {
			return {
				method: 'DELETE',
				actor: {
					url: url.origin,
					name: url.hostname
				}
			};
		} else {
			throw new HttpError(`The request failed (HTTP ${res.status})`);
		}
	}

	const html = await res.text();

	const { document } = new JSDOM(html).window;

	const { author, ...data } = parseDoc(document, sourceUrl);

	data.uri = sourceUrl.href;

	return {
		method: 'POST',
		actor: author,
		data
	};
};
