import parser, { HttpError } from './main.js';

function mockResponse(content) {
	return {
		ok: true,
		text(){
			return content;
		}
	}
}

test('General parser', async ()=>{
	const fetch = ()=>mockResponse(`<article>Hello!<footer><time datetime="2001"></time>Footer</footer></article>`);

	const parse = parser(fetch);

	expect(await parse('http://example.example/')).toMatchSnapshot();
});

test('General parser w/ microformats', async ()=>{
	const fetch = ()=>mockResponse(`<article class="h-entry"><header class="u-author h-card">By <a href="http://author.example/jim" class="u-url"><span class="p-name">Jim</span></a>
	</header>Hello!</article>`);

	const parse = parser(fetch);

	expect(await parse('http://example.example/')).toMatchSnapshot();

});



test('Deleted content', async ()=>{
	const fetch = ()=>({ ok: false, status: 410 })

	const parse = parser(fetch);

	expect(await parse('http://example.example/')).toMatchSnapshot();

});

test('Misc error', async ()=>{
	const fetch = ()=>({ ok: false, status: 400 })

	const parse = parser(fetch);

	await expect(parse('http://example.example/')).rejects.toBeInstanceOf(HttpError);
});
