import {mf2} from 'microformats-parser';

export default function(html, baseUrl) {
	const parsed = mf2(html, { baseUrl });
	return microformats(parsed);
}

export function microformats(parsed) {
	const note = parsed.items.find(item=>item.type.includes('h-entry'))?.properties;

	if(!note) {
		return null;
	}

	const inReplyTo = note['in-reply-to']?.[0]?.properties?.url?.[0];
	const content = note?.content?.[0]?.html;
	const published = new Date(note?.published?.[0]);
	const author = note?.author?.[0]?.properties;
	const data = {
		inReplyTo,
		content,
		published,
		author: author && {
			name: author.name?.[0],
			url: author.url?.[0],
			avatar: author.photo?.[0]?.value
		}
	};

	return data;
}
