import htmlParser from './html.js';
import { JSDOM } from 'jsdom';

test('parser no article', () => {
	const document = {
		querySelector: ()=>null
	};

	expect(htmlParser(document, '')).toBe(null);
});

test('parse article with header & author', () => {
	const {window: {document}} = new JSDOM('<article><header>By <a href="paul" rel=author>Paul</a></header>Content</article>')

	expect(htmlParser(document, 'http://example.com/')).toMatchSnapshot();
});


test('parse article with header & author & empty time', () => {
	const {window: {document}} = new JSDOM('<article><header><time>Yesterday</time> By <a href="paul" rel=author>Paul</a></header>Content</article>')

	expect(htmlParser(document, 'http://example.com/')).toMatchSnapshot();
});


test('parse article with header & author & time', () => {
	const {window: {document}} = new JSDOM('<article><header><time datetime="2001">Yesterday</time> By <address><img src="eg.jpg"><a href="paul" rel=author>Paul</a></address></header>Content</article>')

	expect(htmlParser(document, 'http://example.com/')).toMatchSnapshot();
});
