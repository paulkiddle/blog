function rm(node) {
	node?.remove();
}

export default function(document, baseUrl){
	const article = document.querySelector('article');

	if(!article) {
		return null;
	}

	const inReplyTo = article.querySelector('*[rel~=in-reply-to]');
	rm(inReplyTo);
	const published = article.querySelector('time');
	rm(published);
	const authorLink = article.querySelector('address *[href], *[rel=author]');
	const authorIcon = article.querySelector('address img, *[rel=author] img');
	rm(authorLink);
	rm(authorIcon);

	for(const node of article.querySelectorAll('header, address, footer, aside')){
		node.remove();
	}

	const authorUrl = new URL(authorLink?.href || '.', baseUrl);

	const dateTime = new Date(published?.dateTime);

	const data = {
		inReplyTo: inReplyTo?.href,
		content: article.innerHTML,
		published: isNaN(dateTime) ? void 0 : dateTime,
		author: {
			name: authorLink?.text || authorIcon?.alt || authorUrl.hostname,
			url: authorUrl.href,
			avatar: authorIcon?.src
		}
	};

	return data;
}
