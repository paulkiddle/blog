import ActivityDeserializer from './activity-json.js';

test('Json deserializer', async () => {
	const a = new ActivityDeserializer;
	expect(await a.deserialize(['[ "test" ]'])).toMatchSnapshot()
})