import WM, { toURL, assert, WebmentionDeserializeError } from './webmention';

test('Webmention deserializer', async () => {
	const isValidResource = ()=>true
	const a = new WM(isValidResource);
	expect(await a.deserialize(['source=http://example.com&target=http://example.com/2'])).toMatchSnapshot()
})

test('To url', ()=> {
	expect(toURL('aaa')).toBe(null);
})

test('assert', () => {
	expect(()=>assert(true))
	expect(()=>assert(false, 'msg')).toThrow(WebmentionDeserializeError)
})