import FormDeserializer from './form-urlencoded.js';

test('Form deserializer', async () => {
	const a = new FormDeserializer;
	expect(await a.deserialize(['a=b'])).toMatchSnapshot()
})