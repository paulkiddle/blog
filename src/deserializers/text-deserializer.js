export default class TextDeserializer {
	#hydrate

	constructor(hydrate){
		this.#hydrate = hydrate;
	}

	async deserialize(request) {
		let body ='';
		for await(const chunk of request){
			body+=chunk;
		}

		const data = body;

		return this.#hydrate(data);
	}
}