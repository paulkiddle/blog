import TextDeserializer from './text-deserializer.js';

export default class FormDeserializer extends TextDeserializer{
	constructor(){
		super(text=>this.#hydrate(text));
	}

	async #hydrate(body){
		const data = new URLSearchParams(body);
		return data;
	}
}