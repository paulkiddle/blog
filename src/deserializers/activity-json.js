import TextDeserializer from './text-deserializer.js';

export default class ActivityDeserializer extends TextDeserializer {
	constructor(){
		super(text=>this.#hydrate(text));
	}

	async #hydrate(body){
		const data = JSON.parse(body);
		return data;
	}
}