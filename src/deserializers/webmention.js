import { Webmention } from '../ui/routes/inbox/inbox.js';
import TextDeserializer from './text-deserializer.js';

export function toURL(str){
	try {
		return new URL(str);
	} catch {
		return null;
	}
}

export class WebmentionDeserializeError extends Error {}

export function assert(condition, message) {
	if(!condition) {
		throw new WebmentionDeserializeError(message);
	}
}

export default class WebmentionDeserializer extends TextDeserializer {
	#isValidResource

	constructor(isValidResource){
		super(text=>this.#hydrate(text));
		this.#isValidResource = isValidResource;
	}

	async #hydrate(body){
		assert(body, 'The request has no body.');

		const data = new URLSearchParams(body);
		const source = data.get('source');

		assert(source, 'The request body has no "source" param');

		const sourceURL = toURL(source);
		assert(sourceURL?.protocol?.match(/^https?:$/), 'The source parameter is not a valid http(s) URL');

		const target = data.get('target');
		assert(target, 'The request body has no "target" param');

		const targetURL = toURL(target);
		assert(targetURL?.protocol?.match(/^https?:$/), 'The target parameter is not a valid http(s) URL');
		assert(source !== target, 'The source and target URL must not be the same');

		assert(await this.#isValidResource(targetURL), 'The target URL is not a valid resource');

		return new Webmention({
			source: sourceURL,
			target: targetURL
		});
	}
}

