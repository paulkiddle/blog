import Posts from './index.js';
import knex from 'knex';
import { render, htmlIterator } from 'encode-html-template-tag';

let posts;
beforeEach(async ()=>{
	const sql = knex({
		client: 'sqlite3',
		connection: {
			filename: ':memory:'
		}
	});

	posts = await Posts(sql);
});

test('Create & get posts', async ()=>{
	const url = await posts.create({title:'title', url:'url', content:'content', source:'source'});
	expect(url).toBe('url');
	expect(await posts.get(url)).toMatchObject({title:'title', url:'url', content:'content', source:'source', date: expect.any(Date)});
});

test('Lists posts', async ()=>{
	await posts.create({title:'title', url:'url', content:'content', source:'source'});
	await new Promise(resolve=>setTimeout(resolve, 1000))
	await posts.create({title:'title2', url:'url2', content:'content2', source:'source2'});

	expect(await posts.list()).toMatchObject([
		{title:'title2', url:'url2', content:'content2', source:'source2', date: expect.any(Date)},
		{title:'title', url:'url', content:'content', source:'source', date: expect.any(Date)}
	]);
});

test('Edit posts', async ()=>{
	const url = await posts.create({title:'title', url:'url', content:'content', source:'source'});
	await posts.edit(url, {title: 'title2', content: 'content2', source: 'source2'});
	expect(await posts.get(url)).toMatchObject({title: 'title2', content: 'content2', source: 'source2', url: 'url', date: expect.any(Date)});
});

test('Read & write comments', async ()=>{
	const url = await posts.create({ url: '/url', title: 'title', content: 'content', source: 'source' });
	const profile = { id: 'ex:paul', name: 'Paul', avatar: 'example avatar' };
	const id = await posts.addComment(url, profile, 'comment');
	await posts.approveComment(id);
	const comment = await posts.getComment(id);
	expect(comment).toEqual({
		author: {
			uri: 'ex:paul',
			name: 'Paul',
			avatar: 'example avatar'
		},
		"content": expect.objectContaining({ [htmlIterator]: expect.any(Function) }),
    "created_at": expect.any(String),
		"id": 1,
		"post_id": 1,
		"profile_id": 1,
		"published": 1,
		"uri": null,
    "uuid": expect.any(String)
	});
	expect(comment.content.toString()).toEqual('comment');
	expect(await render(comment.content)).toEqual('comment');
	expect(await posts.getComments(url)).toMatchObject([{ ...comment, content: expect.any(Object) }]);
});

test('Delete comments', async ()=>{
	const url = await posts.create({ url: '/url', title: 'title', content: 'content', source: 'source' });
	const profile = { id: 'ex:paul', name: 'Paul', avatar: 'example avatar' };
	const cid = await posts.addComment(url, profile, 'comment');
	await posts.approveComment(cid);
	expect((await posts.getComments(url)).length).toBe(1);

	await posts.deleteComment(cid);

	expect((await posts.getComments(url)).length).toBe(0);

	await posts.delete(url);
	expect(await posts.get(url)).toBe(undefined);
});
