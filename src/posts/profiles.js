export default class Profiles {
	#sql;

	static create(table){
		table.increments();
		table.string('uri').unique();
		table.string('name');
		table.string('avatar');
	}

	constructor(sql, name) {
		this.#sql = ()=>sql(name);
	}

	async set(uri, { name, avatar=null }) {
		await this.#sql().insert({ uri, name, avatar }).onConflict('uri').merge(['name', 'avatar']);
		const [{id}] = await this.#sql().select('id').where('uri', uri);

		return id;
	}
}
