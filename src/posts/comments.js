import { v4 as uuid } from 'uuid';
import html, { render } from 'encode-html-template-tag';

//const posts = 'posts';
const profiles = 'profiles';
const profile_id = 'profiles.id';
const post_id = 'posts.id';

export default class Comments {
	#sql

	static create(table, knex){
		table.increments().primary();
		table.string('uuid');
		table.integer('profile_id');
		table.foreign('profile_id').references(profile_id);
		table.integer('post_id');
		table.foreign('post_id').references(post_id);
		table.string('comment');
		table.boolean('published');
		table.timestamp('created_at').defaultTo(knex.fn.now());
	}

	static upgrade(table) {
		table.string('uri').unique().defaultTo(null);
	}

	constructor(sql/*, createUri*/){
		this.#sql = sql;
		//this.#createUri = createUri;
	}

	async add({ postId, profileId, content, uri, createdAt }, published=false) {
		const itemId = uuid();
		//uri ?? (uri = this.#createUri(itemId));

		const comment = await render(content);

		await this.#sql('comments').insert({
			uuid: itemId,
			post_id: postId,
			profile_id: profileId,
			comment,
			published,
			uri: uri ?? null,
			created_at: createdAt ?? new Date()
		});

		return itemId;
	}

	async get(publishedOnly, modify) {
		const profileFields = ['uri', 'avatar', 'name'];
		const selectFields = profileFields.map(f=>`${profiles}.${f} as profile_${f}`);

		const stmt = this.#sql('comments')
			.select(['comments.*', ...selectFields])
			.leftJoin(profiles, {[profile_id]:'profile_id'});

		const res = await modify(publishedOnly ? stmt.where({published: true}) : stmt);
		return res.map(r=>{
			const author = {};
			for(const f of profileFields) {
				const pf = 'profile_'+f;
				author[f] = r[pf];
				delete r[pf];
			}

			r.author = author;
			const comment = r.comment;
			r.content = html.safe(comment);
			r.content.toString = ()=>comment;
			delete r.comment;

			return r;
		});
	}

	async approve(uuid) {
		await this.#sql('comments')
			.update({
				published: true
			})
			.where({ uuid });
	}

	async delete(uuid){
		await this.#sql('comments')
			.delete()
			.where({ uuid });
	}

	async deleteAll(postId) {
		await this.#sql('comments')
			.delete()
			.where({ post_id: postId });
	}
}
