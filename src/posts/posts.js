import migrate from 'good-migrations-knex';

const Post = post => ({
	...post,
	date: new Date(post.date)
});

export default async (sql) => {
	await(await migrate(
		sql,
		'posts'))(
		[
			()=>sql.schema.createTable('posts', table => {
				table.increments('id').primary();
				table.string('title');
				table.string('url').unique();
				table.text('content');
				table.timestamp('date');
			}),
			()=>sql.schema.alterTable('posts', table => {
				table.text('source');
			}),
			async()=>{
				if(!await sql.schema.hasColumn('posts', 'id')) {
					await sql.schema.alterTable('posts', table => {
						table.dropPrimary();
						table.unique('url');
						table.increments('id').primary();
						table.timestamp('date').alter();
					});
				}
			}
		]
	);

	return new Posts(sql);
};

export class Posts {
	#sql;

	constructor(sql) {
		this.#sql = sql;
	}

	async list(){
		const sql = this.#sql;
		const posts = await sql.select('*').from('posts').orderBy('date', 'DESC');
		return posts.map(Post);
	}

	async get(url) {
		const sql = this.#sql;
		const [post] = await sql.select('*').from('posts').where({url});
		return post && Post(post);
	}

	async create({title, url, content, source}) {
		const sql = this.#sql;
		await sql('posts').insert({
			title,
			url,
			content,
			source,
			date: new Date().toISOString()
		});
		return url;
	}

	async edit(id, { title, content, source }) {
		const sql = this.#sql;
		return sql('posts').update({title, content, source}).where({url:id});
	}

	async getId(path) {
		const post = await this.get(path);
		return post && post.id;
	}

	async delete(path) {
		const id = await this.getId(path);
		await this.#sql('posts').delete().where({ id });
		return id;
	}
}
