import migrate from 'good-migrations-knex';
import Profiles from './profiles.js';
import migratePosts, { Posts } from './posts.js';
import Comments from './comments.js';

export default async (sql) => {
	await (
		await migrate(sql, 'blog')
	)([
		async ()=>{
			await sql.schema.createTable('profiles', Profiles.create);
			await migratePosts(sql);
			await sql.schema.createTable('comments', t=>Comments.create(t, sql));
		},
		async () => sql.schema.alterTable('comments', t=>Comments.upgrade(t, sql))
	]);

	const profiles = new Profiles(sql, 'profiles');
	const posts = new Posts(sql);
	const comments = new Comments(sql);

	return new BlogStore({ posts, profiles, comments });
};

export class BlogStore {
	#posts;
	#profiles;
	#comments;

	constructor({posts, profiles, comments}) {
		this.#posts = posts;
		this.#profiles = profiles;
		this.#comments = comments;
	}

	async list(){
		return this.#posts.list();
	}

	async get(url) {
		return this.#posts.get(url);
	}

	async create(args) {
		return this.#posts.create(args);
	}

	async edit(id, args) {
		return this.#posts.edit(id, args);
	}

	async addComment(postUri, author, options, published) {
		if(typeof options === 'string') {
			options = {
				content: options
			};
		}
		const { id, ...authorProps } = author;
		const profileId = await this.#profiles.set(id, authorProps);
		const postId = await this.#posts.getId(postUri);
		return this.#comments.add({postId, profileId, content: options.content, uri: options.uri, createdAt: options.createdAt }, published);
	}

	async getComments(uri, user, publishedOnly=true) {
		if(uri==null){
			return [];
		}
		return this.#comments.get(publishedOnly, stmt => {
			const uid = user && user.id;
			if(uid){
				stmt.orWhere({'profiles.uri':user.id});
			}
			return stmt.leftJoin('posts', 'post_id', 'posts.id').where('posts.url', uri);
		});
	}

	async getComment(uuid) {
		const [comment] = await this.#comments.get(false, s=>s.where({ uuid }));
		return comment;
	}

	async getCommentByUri(uri) {
		const [comment] = await this.#comments.get(false, s=>s.where({ 'comments.uri': uri }).limit(1));
		return comment;
	}

	async approveComment(uuid) {
		await this.#comments.approve(uuid);
	}

	async deleteComment(uuid) {
		await this.#comments.delete(uuid);
	}

	async delete(uri){
		const id = await this.#posts.delete(uri);
		await this.#comments.deleteAll(id);
	}
}
