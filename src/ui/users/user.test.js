import User from './user.js';
import { jest } from '@jest/globals'
import { user } from 'sp-templates';

test('Cannot create posts', () => {
	const posts = {
		create: jest.fn()
	}
	const user = new User({posts})
	expect(()=>user.createPost(1,2,3)).toThrow();
})

test('Cannot edit posts', () => {
	const posts = {
		edit: jest.fn()
	}
	const user = new User({posts})
	expect(()=>user.editPost(1,2,3)).toThrow();
})

test('Gets post', async ()=>{
	const post = {title:'title'};
	const posts = {
		get: ()=>post
	}
	const user = new User({posts});
	expect(await user.getPost()).toMatchSnapshot();
})


test('Lists posts', async ()=>{
	const post = {title:'title'};
	const posts = {
		list: ()=>[post]
	}
	const user = new User({posts});
	expect(await user.getPosts()).toMatchSnapshot();
})

test('Can\'t create post', async()=>{
	const user = new User({});
	expect(user.canCreatePost).toBe(false);
})

test('Subclasses can create post', async()=>{
	const user = new (class extends User{
		get createPost(){ return true; }
	}) ({});
	expect(user.canCreatePost).toBe(true);
})

test('Cannot edit post', async()=>{
	const user = new User({});
	expect(()=>user.editPost).toThrow();
})

test('Cannot edit post, create comment, approve comment, or delete comment', async()=>{
	const user = new User({});
	expect([user.canEditPost(), user.canComment(), user.canDeleteComment(), user.canApproveComments ]).toEqual([false, false, false ,false])
})

test('Gets comments', async()=>{
	const comments = ['comment']
	const getComments = jest.fn(()=>comments);

	const user = new User({
		posts: { getComments }
	});

	expect(await user.getPostComments({url:'post-url'})).toEqual(comments);
	expect(getComments).toHaveBeenCalledWith('post-url', user)
})
