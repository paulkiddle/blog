import User from './user.js';
import html, { render, htmlIterator } from 'encode-html-template-tag';
import sanitizeHtml from 'sanitize-html';

const sanitizeDefaults = {
	allowedTags: [ 'b', 'i', 'em', 'strong', 'a' ],
	allowedAttributes: {
		'a': [ 'href' ]
	}
};

export default class AuthenticatedUser extends User {
	authenticated = true;
	#posts
	#notify
	#super
	#sanitizeOptions

	constructor({posts, notify, isSuper, sanitizeOptions=sanitizeDefaults, keypair, enqueue}, id, u){
		super({posts, keypair, enqueue}, comment=>({
			view: this.#isOwnerOf(comment),
			delete: this.#isOwnerOf(comment),
			approve: isSuper
		}));

		this.#super = isSuper;
		this.#posts = posts;
		this.#notify = notify;
		this.#sanitizeOptions = sanitizeOptions;

		this.id = id;
		this.name = u.name;
		this.avatar = u.avatar;
	}

	canEditPost(){
		return false;
	}

	canComment(post){
		return post.commentsOpen;
	}

	#isOwnerOf = (comment) => {
		return this.#super || comment.author.uri === this.id || (new URL(comment.author.uri).origin === this.id);
	}

	canDeleteComment(comment) {
		return this.#isOwnerOf(comment);
	}

	async deleteComment(uuid) {
		const comment = await this.#posts.getComment(uuid);
		if(!this.#isOwnerOf(comment)) {
			throw new Error('You can\'t delete this comment.');
		}
		await this.#posts.deleteComment(uuid);
	}

	async addPostComment(post, comment){
		if(comment && comment[htmlIterator]) {
			comment = html.safe(sanitizeHtml(await render(comment), this.#sanitizeOptions));
		}

		if(!this.canComment(post)) {
			throw new Error('You can\'t comment on this post.');
		}
		await this.#posts.addComment(post.url, this, comment);
		if(this.#notify) {
			this.#notify(
				`New comment on "${post.title}"`,
				`[${this.name}](${this.id}) has added a new comment to [${post.title}](${post.url}), awaiting your approval.`
			);
		}
	}
}
