import Note from './models/note.js';
import Post from './post.js';
import BaseUser from '../../user/user.js';

class ForbiddenError extends Error {
	constructor(message='Forbidden'){
		super(message);
	}
}

export default class User extends BaseUser {
	#posts
	#commentPermissions;
	authenticated = false;

	constructor({posts, keypair, enqueue }, permissions = ()=>({})) {
		super({ posts, keypair, enqueue });

		this.#posts = posts;
		this.#commentPermissions = permissions;
	}

	async getPost(url){
		const post = await this.#posts.get(url);
		return post && new Post(this, post);
	}

	get canCreatePost(){
		try {
			this.createPost;
			return true;
		} catch(e) {
			return false;
		}
	}

	get createPost(){
		throw new ForbiddenError();
	}

	get editPost(){
		throw new ForbiddenError();
	}

	canEditPost() {
		return false;
	}

	canComment() {
		return false;
	}

	canDeletePost(){
		return false;
	}

	deletePost(){
		throw new ForbiddenError();
	}

	canDeleteComment(){
		return false;
	}

	get canApproveComments() {
		return false;
	}

	getPostComments(post) {
		return this.#posts.getComments(post.url, this);
	}

	async getComment(source){
		const { uuid, published, ...comment } = await this.#posts.getCommentByUri(source);
		const { view, ...permissions } = this.#commentPermissions(comment) || {};

		return new Note(this.#posts, uuid, comment, { view: published || view, ...permissions });
	}
}
