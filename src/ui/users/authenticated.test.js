import User from './authenticated.js';
import { jest } from '@jest/globals'

test('Has name & id', ()=> {
	expect(new User({}, 'id', { name: 'name', avatar:'avatar' })).toMatchSnapshot();
})

test('Cannot create posts', () => {
	const posts = {
		create: jest.fn()
	}
	const user = new User({posts}, 'id', {})
	expect(()=>user.createPost(1,2,3)).toThrow();
	expect(posts.create).not.toHaveBeenCalled();
})

test('Cannot edit posts', () => {
	const posts = {
		edit: jest.fn()
	}
	const user = new User({posts}, 'id', {})
	expect(user.canEditPost()).toBe(false);
	const opts = {title: 'title', content:'content', source:'source'}
	expect(()=>user.editPost('id', opts)).toThrow();
	expect(posts.edit).not.toHaveBeenCalled();
})

test('Gets post', async ()=>{
	const post = {title:'title'};
	const posts = {
		get: ()=>post
	}
	const user = new User({posts}, 'id', {});
	expect(await user.getPost()).toMatchSnapshot();
})

test('Can comment if comments open', async()=>{
	const user = new User({posts:{}}, 'id', {});
	expect(user.canComment({ commentsOpen: true })).toBe(true);

	expect(user.canComment({ commentsOpen: false })).toBe(false);
})
