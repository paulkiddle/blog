import Factory from './index.js';
import Authenticated from './authenticated.js';
import Unauthenticated from './user.js'
import Super from './super.js';
import { jest } from '@jest/globals'

test('Unauthenticated user', () => {
	const f = Factory();

	expect(f()).toBeInstanceOf(Unauthenticated);
})

test('Authenticated user', () => {
	const f = Factory();

	expect(f('https://example', {})).toBeInstanceOf(Authenticated);
})

test('Super user', () => {
	const f = Factory();

	expect(f('https://kith.kitchen/users/Paul', {})).toBeInstanceOf(Authenticated);
	expect(f('https://kith.kitchen/users/Paul', {}, 'https://kith.kitchen')).toBeInstanceOf(Super);
})
