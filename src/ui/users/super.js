import AuthenticatedUser from './authenticated.js';

export default class SuperUser extends AuthenticatedUser {
	authenticated = true;
	#posts

	constructor({posts, keypair, enqueue}, id, u){
		super({posts, keypair, isSuper: true, enqueue }, id, u);

		this.#posts = posts;
	}

	createPost(...etc) {
		return this.#posts.create(...etc);
	}

	editPost(id, props){
		return this.#posts.edit(id, props);
	}

	canEditPost(){
		return true;
	}

	canComment(){
		return true;
	}

	canDeleteComment(){
		return true;
	}

	get canApproveComments() {
		return true;
	}

	async approveComment(uuid) {
		await this.#posts.approveComment(uuid);
	}

	addPostComment(post, comment){
		return this.#posts.addComment(post.url, this, comment, true);
	}

	getPostComments(post) {
		return this.#posts.getComments(post.url, null, false);
	}

	canDeletePost(){
		return true;
	}

	deletePost(post) {
		return this.#posts.delete(post.url);
	}
}
