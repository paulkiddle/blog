const WEEK = 604800000;

export default class Post {
	#user

	constructor(user, options){
		this.#user = user;
		const { date, ...o } = options;
		Object.assign(this, o);

		this.date = new Date(date || 0);
	}

	get commentsOpen(){
		return Date.now() - this.date < WEEK;
	}

	get canComment(){
		return this.#user.canComment(this);
	}

	get editable() {
		return this.#user.canEditPost(this);
	}

	async addComment(body){
		return this.#user.addPostComment(this, body);
	}

	async getComments(){
		return (await this.#user.getPostComments(this)).map(
			comment => ({
				...comment,
				approvable: (!comment.published) && this.#user.canApproveComments,
				deletable: this.#user.canDeleteComment(comment)
			})
		);
	}

	async approveComment(uuid) {
		await this.#user.approveComment(uuid);
	}

	async deleteComment(uuid) {
		await this.#user.deleteComment(uuid);
	}

	get deletable(){
		return this.#user.canDeletePost(this);
	}

	delete(){
		return this.#user.deletePost(this);
	}
}
