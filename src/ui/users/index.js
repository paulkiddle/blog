import Visitor from './user.js';
import AuthenticatedUser from './authenticated.js';
import Super from './super.js';

export default (posts, notify, keypair, enqueue) => {
	const visitor = new Visitor({posts, keypair, enqueue});

	function getUser(id, u, authority) {
		if(u && id === 'https://kith.kitchen/users/Paul' && authority === 'https://kith.kitchen') {
			return new Super({posts, keypair, enqueue}, id, u);
		} else if(u && id) {
			return new AuthenticatedUser({posts, notify, keypair, enqueue}, id, u);
		}

		return visitor;
	}

	getUser.visitor = visitor;

	return getUser;
};
