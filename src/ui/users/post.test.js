import Post from './post.js';
import { jest } from '@jest/globals'

test('Can comment check', ()=> {
	const user = {
		canComment(){ return true; }
	};
	const post = new Post(user, {});
	expect(post.canComment).toBe(true);
})

test('Can edit check', ()=> {
	const user = {
		canEditPost(){ return false; }
	};
	const post = new Post(user, {});
	expect(post.editable).toBe(false);
})

test('Add comment', async ()=> {
	const addPostComment = jest.fn();

	const user = {
		addPostComment
	};
	const post = new Post(user, {});
	await post.addComment('body')
	expect(addPostComment).toHaveBeenCalledWith(post, 'body');
})

test('Get comments', async ()=> {
	const comments = [
		{
			a: 1,
			published: true
		},
		{
			b: 2,
			published: false
		}
	];
	const getPostComments = jest.fn(()=>comments);
	const canApproveComments = true;
	const canDeleteComment = jest.fn(c=>!!c.a);

	const user = {
		getPostComments,
		canApproveComments,
		canDeleteComment
	};
	const post = new Post(user, {});
	expect(await post.getComments()).toMatchSnapshot();
})

test('Approve comment', async ()=> {
	const approveComment = jest.fn();

	const user = {
		approveComment
	};
	const post = new Post(user, {});
	await post.approveComment('id')
	expect(approveComment).toHaveBeenCalledWith('id');
})

test('Delete comment', async ()=> {
	const deleteComment = jest.fn();

	const user = {
		deleteComment
	};
	const post = new Post(user, {});
	await post.deleteComment('id')
	expect(deleteComment).toHaveBeenCalledWith('id');
})

test('Comments closed', async ()=> {
	const user = {};
	const post = new Post(user, {
		date: new Date(Date.now()-604800001)
	});
	expect(post.commentsOpen).toBe(false);
})


test('Comments open', async ()=> {
	const user = {};
	const post = new Post(user, {
		date: new Date(Date.now())
	});
	expect(post.commentsOpen).toBe(true);
})
