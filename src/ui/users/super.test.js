import User from './super.js';
import { jest } from '@jest/globals'

test('Has name & id', ()=> {
	expect(new User({}, 'id', { name: 'name', avatar:'avatar' })).toMatchSnapshot();
})

test('Creates posts', () => {
	const posts = {
		create: jest.fn()
	}
	const user = new User({posts}, 'id', {})
	user.createPost(1,2,3);
	expect(posts.create).toHaveBeenCalledWith(1,2,3);
})

test('Edits posts', () => {
	const posts = {
		edit: jest.fn()
	}
	const user = new User({posts}, 'id', {})
	const opts = {title: 'title', content:'content', source:'source'}
	user.editPost('id', opts);
	expect(posts.edit).toHaveBeenCalledWith('id', opts);
})

test('Gets post', async ()=>{
	const post = {title:'title'};
	const posts = {
		get: ()=>post
	}
	const user = new User({posts}, 'id', {});
	expect(await user.getPost()).toMatchSnapshot();
})


test('Can edit post, create comment, approve comment, or delete comment', async()=>{
	const user = new User({}, 'id', {});
	expect([user.canEditPost(), user.canComment(), user.canDeleteComment(), user.canApproveComments ]).toEqual([true, true, true ,true])
})

test('Approves comment', () => {
	const posts = {
		approveComment: jest.fn()
	}
	const user = new User({posts}, 'id', {})
	user.approveComment(1);
	expect(posts.approveComment).toHaveBeenCalledWith(1);
})

test('Adds comment', () => {
	const posts = {
		addComment: jest.fn()
	}
	const user = new User({posts}, 'id', {})
	user.addPostComment({url:1}, 'comment');
	expect(posts.addComment).toHaveBeenCalledWith(1, user, 'comment', true);
})

test('Gets comments', () => {
	const posts = {
		getComments: jest.fn()
	}
	const user = new User({posts}, 'id', {})
	user.getPostComments({url:1});
	expect(posts.getComments).toHaveBeenCalledWith(1, null, false);
})
