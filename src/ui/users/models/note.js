export default class Note {
	#id
	#permissions
	#store

	constructor(store, id, data, permissions = {}){
		this.#store = store;
		this.#id = id;
		this.#permissions = permissions;

		if(!permissions.view) {
			throw new TypeError('You do not have permission to view this note.');
		}

		Object.assign(this, data);
	}

	get deletable(){
		return !!this.#permissions.delete;
	}

	async delete(){
		if(this.#permissions.delete) {
			await this.#store.deleteComment(this.#id);
		} else {
			throw new TypeError('You cannot delete this note.');
		}
	}

	/*

	get approvable(){
		return this.#permissions.approve;
	}

	async approve() {
		if(this.#permissions.approve){
			await this.#store.approve(this.#id);
		} else {
			throw new TypeError('You cannot approve this note.')
		}
	}
	*/
}
