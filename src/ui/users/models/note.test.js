import Note from './note.js';
import { jest } from '@jest/globals';

class Store {
	deleteComment = jest.fn()
}

test('Needs view permission', () => {
	expect(()=>new Note({}, 1, {}, {})).toThrow(TypeError);
	expect(new Note({}, 1, {}, { view: true })).toBeInstanceOf(Note);
});

test('Delete throws with no permission', async () => {
	const store = new Store;
	const note = new Note(store, 1, {}, { view: true });

	expect(note.deletable).toBe(false);
	await expect(note.delete()).rejects.toBeInstanceOf(TypeError);
	expect(store.deleteComment).not.toHaveBeenCalled();
})

test('Delete succeeds with permission', async () => {
	const store = new Store;
	const note = new Note(store, 1, {}, { view: true, delete: true });

	expect(note.deletable).toBe(true);
	await note.delete();
	expect(store.deleteComment).toHaveBeenCalledWith(1);
})
