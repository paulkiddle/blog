import GpodRequest from 'gpod-request';
import mime from 'mime-types';

export default class Request extends GpodRequest {
	#req

	static url(request){
		return GpodRequest.url(request);
	}

	constructor(req) {
		super(req);

		this.#req = req;

		this.accept = req.headers.accept || '*/*;q=0.8';

		const m = this.path.match(/\.[a-z0-9]+$/i);
		if(m){
			const type = mime.lookup(m[0]) || '*/*';
			this.accept = type + ',' + this.accept;
			this.path = this.path.slice(0, m.index);
		}
	}

	get headers() {
		return {
			accept: this.accept
		};
	}

	get req(){
		return this.#req;
	}

	toString(props={}){
		if(typeof props === 'string') {
			props = {
				path: props,
				method: 'GET'
			};
		}
		return Request.url({ ...this, ...props });
	}

	redirect(props){
		return res => {
			res.statusCode = 303;
			res.setHeader('Location', this.toString(props));
		};
	}

	static delete(path){
		const url = Request.url({
			path,
			method: 'DELETE'
		});

		return {
			url,
			method: 'POST'
		};
	}
}
