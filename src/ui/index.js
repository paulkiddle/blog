import template from './template.js';
import routes from './routes/index.js';
import Request from './request.js';
import html from 'encode-html-template-tag';

function $404(res){
	res.statusCode = 404;
	return html`<div>Page not found</div>`;
}

function errorRoute(/*error*/) {
	return res => {
		res.statusCode = 500;
		return html`<div>Internal server error</div>`;
	};
}


export default ({ webmentionEndpoint, origin, auth, ...options }) => {
	const getAuthUrl = u=>auth.getRedirectUrl(u);
	const blogFeed = new URL('/feed', origin).href;

	const router = routes({ title: options.title, origin, getAuthUrl });

	async function route(req, user){
		try {
			const route = await router(req, user);
			if(route === false) {
				return $404;
			}
			return route;
		} catch(e) {
			console.warn(e);
			return errorRoute(e);
		}
	}

	return async (req, res) => {
		const user = auth.getUser(req);

		if(await auth.handleCallback(req, res)) {
			res.setHeader('location', '/');
			res.statusCode = 302;
			res.end();
			return;
		}

		res.setHeader('Link', `<${webmentionEndpoint}>; rel="webmention"`);

		const request = new Request(req);

		const renderTemplate = body => template({...options, blogFeed, user}, body).generate();
		const serialize = user.getSerializer(request, res, renderTemplate);

		const url = new URL(request.url, origin);

		try {
			const resource = await user.getResource(url);

			if(resource){
				if(request.method === 'GET') {
					return serialize(resource);
				}
				if(request.method === 'POST') {
					const body = await user.getBody();
					const created = await resource.append(body);
					return serialize.created(created);
				}

				return serialize.methodNotAllowed();
			}
		} catch(e) {
			return serialize(e);
		}

		const response = await route(request, user);

		const body = typeof response === 'function' ? await response(res) : response;

		if(body) {
			res.write(await template({...options, blogFeed, user}, body).render());
		}
		res.end();
	};
};
