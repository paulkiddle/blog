import feed from './feed.js';
import post from './post/router.js';
import Auth from './auth/index.js';
import Create from './post-author/create.js';
import Edit from './post-author/edit.js';

export const menu = {
	home: '/',
	feed: '/feed',
	auth: '/auth',
	createPost: '/post'
};

export function redirect(url) {
	return res => {
		res.setHeader('location', url);
		res.statusCode = 302;
		res.end();
	};
}

export default (options) => {
	const auth = Auth(async url => redirect(await options.getAuthUrl(url)));
	const createPost = Create(redirect);
	const editPost = Edit(redirect);

	return async (request, user) => {
		const req = request.req;

		if(request.method !== 'DELETE') {
			switch(request.path) {
			case menu.feed:
				return feed({ title: options.title, feedUrl: options.origin + menu.feed, url: options.origin }, user);
			case menu.auth:
				return auth(req);
			case menu.createPost:
				return createPost(request, user.createPost.bind(user));
			}
		}

		const postData = await user.getPost(request.path);
		if(!postData) {
			return false;
		}
		if(request.method === 'OPTIONS'){
			return editPost(request, postData, user.editPost.bind(user));
		} else if (request.method === 'DELETE') {
			await postData.delete();
			return request.redirect('/');
		} else {
			return post(request, postData, request.toString({ method:'OPTIONS' }));
		}
	};
};
