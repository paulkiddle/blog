import feed from './feed';
import {jest} from '@jest/globals';

test('feed route', async()=>{
	const options = {
		title: 'Feed title',
		feedUrl: 'http://example.com/feed',
		url:  'http://example.com'
	};
	const session = {
		async getPosts(){
			return [{
				title: 'Post title',
				url: '/post',
				date: new Date('2021-01-01'),
				content: '<p>Example content</p>'
			}];
		}
	}

	const handler = await feed(options, session);

	const res = {
		setHeader: jest.fn(),
	}
	const response = new Promise(resolve => {
		res.end = resolve;
	});

	await handler(res);

	expect(res.setHeader).toHaveBeenCalledWith('content-type', 'application/rss+xml');

	expect((await response).replace(/<lastBuildDate>[^<]+<\/lastBuildDate>/, '<lastBuildDate removed for tests />')).toMatchSnapshot();
})
