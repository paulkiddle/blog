import Router, { redirect } from './index.js';
import {jest} from '@jest/globals';

test('redirect', ()=>{
	const res = {
		setHeader: jest.fn(),
		end: jest.fn()
	}
	redirect('/')(res);
	expect(res.end).toHaveBeenCalled();
});

test('feed route', async()=>{
	const router = Router({});

	const req = {
		path: '/feed'
	}
	const user = {
		getPosts(){
			return [];
		}
	}

	expect((await router(req, user)).name).toBe('feed');
})

test('create route', async()=>{
	const router = Router({});

	const req = {
		path: '/post'
	}
	const user = {
		getPosts(){
			return [];
		},
		createPost(){}
	}

	expect((await router(req, user))).toMatchSnapshot();
})

test('delete route', async()=>{
	const router = Router({});

	const req = {
		path: '/post',
		method: 'DELETE',
		redirect: a=>a
	}
	const post = {
		delete: jest.fn()
	}
	const user = {
		getPost(){
			return post
		},
		getPosts(){
			return [];
		},
		createPost(){}
	}

	expect((await router(req, user))).toMatchSnapshot();
	expect(post.delete).toHaveBeenCalled();
})

test('get no post', async()=>{
	const router = Router({});
	expect(await router({}, { getPost(){} })).toBe(false);
})