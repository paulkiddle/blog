import Inbox, { Webmention } from './inbox.js';
import { jest } from '@jest/globals';

test('Inbox append', async()=>{
	const enqueue = jest.fn();	
	const webmention = new Webmention({ 
		source: 'source',
		target: 'target'
	});

	const i = new Inbox({ enqueue });

	expect(await i.append(webmention)).toBe(true);
	expect(enqueue.mock.calls).toMatchSnapshot();
	expect(await i.append({})).toMatchSnapshot();
});