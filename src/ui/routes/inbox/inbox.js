export class Webmention {
	constructor({ source, target }) {
		this.source = source;
		this.target = target;
	}
}

export default class Inbox {
	#enqueue;

	constructor({ enqueue }){	
		this.#enqueue = enqueue;
	}

	async append(body){
		if(body instanceof Webmention) {
			const { source, target } = body;
			await this.#enqueue({
				type: 'webmention',
				body: {
					source,
					target
				}
			});

			return true;
		}
	
		console.log('received', body);

		return {
			id: 'http://example.com'
		};
	}
}
