import {view} from './index.js';

test('home handler', async () => {
	const p = [{
		url: '/post1',
		title: 'post 1',
		date: new Date('2020-12-29')
	}];
	expect(await (await view( { posts: p, feed: '/feed' })).render()).toMatchSnapshot();
});

test('deletable post tpl', async () => {
	const p = [{
		url: '/post1',
		title: 'post 1',
		date: new Date('2020-12-29'),
		deletable: true
	}];
	expect(await (await view({ posts:p, feed: '/feed' })).render()).toMatchSnapshot();
});
