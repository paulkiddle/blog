export default class Blog {
	#posts
	#id
	#username;
	#keypair;
	#paths

	constructor(posts, { uri, username, keypair, inbox, feedUrl }){
		this.#posts = posts;
		this.#id = uri;
		this.#username = username;
		this.#keypair = keypair || {};
		this.#paths = { inbox, feed: feedUrl };
	}

	get publicKey(){
		return this.#keypair.public;
	}

	get username(){
		return this.#username;
	}

	get id(){
		return this.#id;
	}

	get posts(){
		return this.#posts;
	}

	get pathname(){
		return new URL(this.#id).pathname;
	}

	get inbox(){
		return this.#paths.inbox;
	}

	get feed(){
		return this.#paths.feed;
	}
}
