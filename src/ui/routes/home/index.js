import html from 'encode-html-template-tag';
import {time} from 'sp-templates';
import deleteForm from '../../delete-form.js';

export function view(site) {
	const posts = site.posts;
	return html`
		<p><a href="${site.feed}">Subscribe via RSS</a></p>
		<h2>Posts</h2>
		<ul>${posts.map(post => html`
			<li>
				<a style="font-weight:bold; font-size: 20px;" href="${post.url}">${post.title}</a>
				<i>${time(post.date)}</i>
				${post.deletable ? deleteForm({ style: 'display:inline', url: post.url }) :	''}
			</li>`)}
		</ul>`;
}