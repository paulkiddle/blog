import Blog from './blog.js';

test('Blog resource', ()=>{
	const blog = new Blog([1,2], { uri: 'file:///', username: 'Paul', keypair: { public: 'public' }, inbox: '/inbox', feedUrl: '/feed' });

	expect(blog.posts).toEqual([1,2]);
	expect(blog.id).toBe('file:///');
	expect(blog.pathname).toBe('/');
	expect(blog.username).toBe('Paul');
	expect(blog.publicKey).toMatchSnapshot();
	expect(blog.inbox).toMatchSnapshot();
	expect(blog.feed).toMatchSnapshot();
});