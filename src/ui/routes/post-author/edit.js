import view from './view.js';

export default redirect => async (req, post, editPost) => {
	if(req.method === 'POST') {
		const { title, source, content } = view.parse(await req.getData());
		await editPost(post.url, { title, source, content });
		return redirect(post.url);
	}

	return view(post);
};
