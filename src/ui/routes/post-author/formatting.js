import html from 'encode-html-template-tag';

export default html`<script>

function rangeInvert(range, relativeTo){
	const before = new Range();
	before.setStartBefore(relativeTo.firstChild);
	before.setEnd(range.startContainer, range.startOffset);

	const after = new Range();
	after.setStart(range.endContainer, range.endOffset);
	after.setEndAfter(relativeTo.lastChild);

	return [before, after];
}

function unformat(range, tag){
	const container = range.commonAncestorContainer;
	const parentEl = container.closest ? container : container.parentElement;
	const tagNode = parentEl.closest(tag);
	const [before, after] = rangeInvert(range, tagNode);
	const inserted = [before, after].map(range => {
		const frag = range.extractContents();
		const el = document.createElement(tag);
		el.appendChild(frag);
		range.insertNode(el);
		return el;
	});
	tagNode.replaceWith(...tagNode.childNodes);
	range.setStartAfter(inserted[0]);
	range.setEndBefore(inserted[1]);
	return range;
}

	function wrapInline(tag){
		const sel = window.getSelection();

		for(let i=0; i<sel.rangeCount; i++){
			const range = sel.getRangeAt(i);
			const frag = range.extractContents();
			for(const node of frag.querySelectorAll(tag)) {
				node.replaceWith(...node.childNodes)
			}
			const el = document.createElement(tag);
			el.appendChild(frag);
			range.insertNode(el);
			range.selectNodeContents(el);
		}
	}
	</script>`;
