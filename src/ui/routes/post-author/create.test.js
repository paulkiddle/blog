import {jest} from '@jest/globals';
import Route from './create.js';
import view from './view.js';
import MockRequest from './mock-request.test.js';

test('Returns view on GET', async() => {
	const redirect = jest.fn();
	const createPost = jest.fn()
	const route = Route(redirect);

	const req = {};

	const rtn = await route(req, createPost);
	expect(rtn).toEqual(view());
	expect(createPost).not.toHaveBeenCalled();
});

test('Runs callback on POST', async() => {
	const createPost = jest.fn((title, url) => ({url}));
	const redirect = jest.fn(()=>'Return value')
	const route = Route(redirect);
	const data = {
		title: 'title',
		content: 'content'
	}
	const parsedResponse = {
		title: 'title',
		url: '/title',
		source: 'content',
		content: '<p>content</p>\n'
	}

	const req = new MockRequest({
		data,
		method: 'POST'
	});


	const rtn = await route(req, createPost);
	expect(rtn).toMatchSnapshot();
	expect(createPost).toHaveBeenCalledWith(parsedResponse);
});
