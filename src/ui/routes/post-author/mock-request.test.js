export default class MockRequest {
	#data

	constructor(o = {}){
		const { data, ...options } = o;
		this.#data = data;
		Object.assign(this, options);
	}
	async getData(){
		return new URLSearchParams(this.#data);
	}
}

test('Dummy', ()=>expect(true).toBe(true));
