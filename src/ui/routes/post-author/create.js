import view from './view.js';

export default redirect => async (req, createPost) => {
	if(req.method === 'POST') {
		const post = view.parse(await req.getData());
		const createdUrl = await createPost(post);
		return redirect(createdUrl);
	}

	return view();
};
