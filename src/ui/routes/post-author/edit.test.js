import {jest} from '@jest/globals';
import Route from './edit.js';
import view from './view.js';
import MockRequest from './mock-request.test.js';

test('Returns view on GET', async() => {
	const redirect = jest.fn();
	const editPost = jest.fn()
	const route = Route(redirect);

	const req = {};

	const rtn = await route(req, {}, editPost);
	expect(rtn).toEqual(view());
	expect(editPost).not.toHaveBeenCalled();
});

test('Runs callback on POST', async() => {
	const editPost = jest.fn();
	const redirect = jest.fn(()=>'Return value')
	const route = Route(redirect);
	const post = {
		url: 'url'
	}
	const parsedResponse = {
		title: 'title',
		content: 'content'
	}

	const req = new MockRequest({
		data: parsedResponse,
		method: 'POST'
	});

	view.parse = jest.fn(()=>parsedResponse);

	const rtn = await route(req, post, editPost);
	expect(rtn).toMatchSnapshot();
	expect(view.parse).toHaveBeenCalledWith(expect.any(URLSearchParams));
	expect(editPost).toHaveBeenCalledWith(post.url, parsedResponse);
});
