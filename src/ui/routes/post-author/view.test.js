import {Readable} from 'stream';
import view from './view.js';

test('Renders post create view', async() => {
	expect(await view().render()).toMatchSnapshot();
});

test('Parses submitted request', async() => {
	const request = new URLSearchParams('content=This%20is%20the%20content&title=And%20this%20is%20title!');
	expect(await view.parse(request)).toMatchSnapshot();
});
