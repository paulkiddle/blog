import html from 'encode-html-template-tag';
import marked from 'marked';
import formatting from './formatting.js';

const controls = html`
	<button type="button" onclick="document.execCommand('bold');">Bold</button>
	<button type="button" onclick="document.execCommand('italic');">Italic</button>
	<button type="button" onclick="const link=prompt('href', '');link && document.execCommand('createLink',null,link)">Link</button>
	${formatting}
	<button type="button" onclick="wrapInline('code');document.querySelector('html-area').dispatchEvent(new InputEvent('input'));">Code</button>
	`;

const form = (post={})=>html`<form method="POST">
	<label>Title<input name="title" value="${post.title||''}"></label><br>
	<script type="module">
	import 'https://unpkg.com/element-internals-polyfill@0.1.30/dist/index.js';
	import HtmlArea from 'https://paulkiddle.gitlab.io/html-area/html-area.js';
	HtmlArea.register();
	</script>
	${controls}
	<label>Content<br>
	<html-area style="display: block;"><textarea name="content" rows="25" cols="100">${post.source||post.content||''}</textarea></html-area>
	</label><br>
	<button>Submit</button>
</form>`;

form.parse = body=>({
	source: body.get('content'),
	content: marked(body.get('content')),
	title: body.get('title'),
	url: '/'+body.get('title').toLowerCase().replace(/[^a-z0-9\s]+/g, '').replace(/\s/g, '-')
});

export default form;
