import html, { element } from 'encode-html-template-tag';
import {time, note, hEntry, hCard} from 'sp-templates';

const commentStyles= html`<style>
.Comment {
	display: flex;
}
</style>`;

export default (post, editUrl) => hEntry({}, content => html`
	<header>
		<h1 class="p-name">${post.title}</h1>
		Posted ${time.published(post.date)} by ${hCard({url: '/', name: 'Paul Kiddle'})}
		${post.editable ? html`<a href="${editUrl}">Edit</a>` : ''}
	</header>

	${content(html.safe(post.content))}

	<aside>
		${post.canComment ? commentForm() : (post.commentsOpen ? html`<a href="/auth">Log in with Mastodon to comment.</a>` : '')}

		${commentStyles}
		${post.getComments().then(c=>c.map(c=>note({
		author: { url: c.author.uri, ...c.author },
		date: c.created_at,
		footer: html`
				${!c.published ? html`<em>unpublished, pending approval</em>`:''}
				${commentActions(c)}
			`,
		id: `comment-${c.uuid}`
	}, c.content)
	))}
	</aside>
`);

function commentForm(){
	const attrs = { method:'POST' };
	return element('form', attrs, html`
		<label>
			Add a comment<br>
			<textarea name="comment"></textarea>
		</label><br>
		<button name="method" value="addComment">Submit</button>
	`);
}
export function parseComment(data) {
	return data.get('comment');
}

function actionButton(method, text) {
	return html`<button name="method" value="${method}">${text}</button>`;
}

function commentActions(c){
	return html`<form method="POST">
		<input type="hidden" name="comment" value="${c.uuid}">
		${c.approvable ? actionButton('approveComment', 'Approve') : ''}
		${c.deletable ? actionButton('deleteComment', 'Delete') : ''}
	</form>`;
}
commentActions.parse = data => data.get('comment');

const parsers = Object.assign(Object.create(null), {
	addComment: parseComment,
	approveComment: commentActions.parse,
	deleteComment: commentActions.parse
});

export function parse(data) {
	const method = data.get('method');
	const parse = parsers[method];
	return parse && {
		method,
		params: parse(data)
	};
}
