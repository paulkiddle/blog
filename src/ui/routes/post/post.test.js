import Post, { parse } from './post.js';
import {jest} from '@jest/globals';

test('post route, comments', async()=>{
	const post = {
		title: 'Post title',
		url: 'http://example.com/post',
		date: new Date('2021-01-01'),
		content: '<p>Example content</p>',
		async getComments(){
			return [];
		}
	};

	expect(await Post(post).render()).toMatchSnapshot();
})

test('post route, comments open', async()=>{
	const post = {
		title: 'Post title',
		url: 'http://example.com/post',
		date: new Date('2021-01-01'),
		content: '<p>Example content</p>',
		async getComments(){
			return [];
		},
		commentsOpen: true
	};

	expect(await Post(post).render()).toMatchSnapshot();
})

test('post route, user can comment', async()=>{
	const post = {
		title: 'Post title',
		url: 'http://example.com/post',
		date: new Date('2021-01-01'),
		content: '<p>Example content</p>',
		async getComments(){
			return [];
		},
		canComment: true,
		editable: true
	};

	expect(await Post(post, '/post/edit').render()).toMatchSnapshot();
})

test('post route, with comments', async()=>{
	const post = {
		title: 'Post title',
		url: 'http://example.com/post',
		date: new Date('2021-01-01'),
		content: '<p>Example content</p>',
		async getComments(){
			return [{
				author: {
					name: 'Paul',
					uri: 'http://example.com'
				},
				created_at: new Date('2021'),
				published: false,
				content: 'ok!',
				uuid: 1,
				approvable: true,
				deletable: true
			},{
				author: {
					name: 'Paul2',
					uri: 'http://example.com/2'
				},
				created_at: new Date('2021'),
				published: true,
				content: 'ok!2',
				uuid: 2,
				approvable: false,
				deletable: false
			}];
		},
		canComment: true
	};

	expect(await Post(post).render()).toMatchSnapshot();
})

test('Parse forms', async()=>{
	const postData = new URLSearchParams({
		method: 'addComment',
		comment: 'my comment'
	});

	expect(parse(postData)).toEqual({
		method: 'addComment',
		params: 'my comment'
	});

	const approveData = new URLSearchParams({
		method: 'approveComment',
		comment: 'my comment'
	});

	expect(parse(approveData)).toEqual({
		method: 'approveComment',
		params: 'my comment'
	});

	expect(parse(new URLSearchParams({ method: 'no'}))).toBeUndefined();
})
