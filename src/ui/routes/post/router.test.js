import router from './router.js';
import {jest} from '@jest/globals';

test('Get route', async()=>{
	expect(await router({ method: 'GET', getData(){} }, { date: new Date('2021'), async getComments(){return []} })).toMatchSnapshot()
})

test('Post route', async()=>{
	expect(await router({ method: 'POST', redirect(a){return a}, getData(){return new URLSearchParams('method=addComment')} }, { date: new Date('2021'), async getComments(){return []}, addComment(){} })).toMatchSnapshot()
})