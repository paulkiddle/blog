import view, { parse } from './post.js';

export default async function(req, post, editUrl) {
	const { method } = req;
	const data = await req.getData();

	if(method === 'GET') {
		return view(post, editUrl);
	} else {
		const { method, params } = parse(data);
		switch(method) {
		case 'addComment':
			await post.addComment(params);
			break;
		case 'approveComment':
			await post.approveComment(params);
			break;
		case 'deleteComment':
			await post.deleteComment(params);
			break;
		}
		return req.redirect({ method: 'GET' });
	}
}
