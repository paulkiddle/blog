import Rss from 'rss';

// https://www.npmjs.com/package/rss
export default async (options, session) => {
	const rss = new Rss({
		title: options.title,
		feed_url: options.feedUrl,
		site_url: options.url,
	});

	for(const post of await session.getPosts()){
		const url = new URL(post.url, options.url).href;
		rss.item({
			title: post.title,
			url,
			guid: url,
			date: post.date.toISOString(),
			description: post.content
		});
	}

	return function feed(res){
		res.setHeader('content-type', 'application/rss+xml');
		res.end(rss.xml());
	};
};
