import {jest} from '@jest/globals';
import Route from './index.js';
import view from './view.js';

test('Returns view on GET', async() => {
	const cb = jest.fn();
	const route = Route(cb);

	const req = {};

	const rtn = await route(req);
	expect(rtn).toBe(view);
	expect(cb).not.toHaveBeenCalled();
});

test('Runs callback on POST', async() => {
	const cb = jest.fn(()=>'Return value');
	const route = Route(cb);
	const parsedResponse = Symbol('Parsed response');

	const req = {
		method: 'POST'
	};

	view.parse = jest.fn(()=>parsedResponse);

	const rtn = await route(req);
	expect(rtn).toMatchSnapshot();
	expect(view.parse).toHaveBeenCalledWith(req);
	expect(cb).toHaveBeenCalledWith(parsedResponse);
});
