import html from 'encode-html-template-tag';
import form from '../../../../form/index.js';

export default form(
	form => form({ novalidate: true }, html`
		<label>
			Log in with Mastodon, enter your instance URL here
			<br>
			<input type="url" name="url">
		</label>
		<button>Log In</button>
	`),
	body => {
		const url = body.get('url');
		if(url.match(/^(?!https:)[^:.]+:/)) {
			throw new Error('Must be an HTTPS url');
		}

		if(!url.startsWith('https:')){
			return 'https://' + url.split('@').pop();
		}

		return url;
	}
);
