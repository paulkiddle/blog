import {Readable} from 'stream';
import view from './view.js';

test('Renders post create view', async() => {
	expect(await view.render()).toMatchSnapshot();
});

test('Parses submitted request', async() => {
	const request = Readable.from(['url=https://example.com'])
	expect(await view.parse(request)).toMatchSnapshot();
});

test('Prepends https', async() => {
	const request = Readable.from(['url=example.com'])
	expect(await view.parse(request)).toMatchSnapshot();
});

test('Rejects http', async() => {
	const request = Readable.from(['url=http://example.com'])
	expect(view.parse(request)).rejects.toBeInstanceOf(Error);
});
