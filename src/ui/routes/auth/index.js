import view from './view.js';

export default action => async req => {
	if(req.method === 'POST') {
		return action(await view.parse(req));
	}

	return view;
};
