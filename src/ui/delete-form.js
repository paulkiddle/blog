import html, { element } from 'encode-html-template-tag';
import Request from './request.js';

export default ({ url, ...etc }, children = html`<button>Delete</button>`) => {
	const r = Request.delete(url);
	return element('form', {
		...etc,
		method: r.method,
		action: r.url
	}, children);
};