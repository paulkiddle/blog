import html from 'encode-html-template-tag';
import {page} from 'sp-templates';

export default ({user, blogFeed, ...options}, ...etc) => page({
	...options,
	head: html`<link rel="alternate" type="application/rss+xml" title="Blog Feed" href="${blogFeed}" />`,
	controls: [
		user.authenticated ?
			html`<div>
				<div class="User">
					<img src="${user.avatar}" alt="" height="20">
					<a href="${user.id}">${user.name}</a>
				</div>
				${user.canCreatePost ? html`<a href="/post">New post</a>` : ''}
			</div>`
			:
			html`<a href="/auth">Login</a>`
	]
}, ...etc);
