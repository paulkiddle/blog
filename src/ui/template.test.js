import template from './template.js';
import { jest } from '@jest/globals'

test('Template', async ()=>{
	const user = {
		authenticated: true
	}

	expect(await template({user}).render()).toMatchSnapshot()
});
