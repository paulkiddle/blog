import Ui from './index.js';
import { jest } from '@jest/globals';

class Res {
	write = jest.fn()
	end = jest.fn()
	setHeader = jest.fn()
}

class Req {
	constructor(url){
		this.url = url;
	}
	headers = {}
}

class User {
	getPost(){}
	getResource(){}
	getSerializer(req, res, tpl){
		return e=>res.write(e.toString())
	}
}

test('ui 404 route', async() => {
	const user = new User
	const ui = await Ui({
		origin: 'http://example.com',
		auth: {
			getUser(){
				return user;
			},
			handleCallback(){}
		}
	});
	const req = new Req('/404');
	const res = new Res;

	await ui(req, res);

	expect(res.write.mock.calls).toMatchSnapshot();
});


test('ui error catch', async() => {
	const user = new User;
	user.getResource = ()=>{throw new Error('Error')}
	const ui = await Ui({
		origin: 'http://example.com',
		auth: {
			getUser(){
				return user;
			},
			handleCallback(){}
		}
	});
	const req = new Req;
	const res = new Res;

	await ui(req, res);

	expect(res.write.mock.calls).toMatchSnapshot();
});
