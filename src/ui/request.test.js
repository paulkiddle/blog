import Request from './request.js';
import stream from 'stream';
import { jest } from '@jest/globals'

// test('Request to url', ()=>{
// 	expect(Request.url({ path: '/home' })).toEqual('/home');
// 	expect(Request.url({ path: '/home', method: 'OPTIONS' })).toEqual('/home!');

// 	expect(new Request({ url: '/a' }).toString('/b')).toEqual('/b')
// });

// test('Stream to search params', async ()=>{
// 	const req = stream.Readable.from(['a=b&c=d']);
// 	req.url = '/';

// 	const r = new Request(req);

// 	const p = await r.getData();
// 	expect(p).toBeInstanceOf(URLSearchParams);
// 	expect(p.toString()).toBe('a=b&c=d')
// });

test('Redirect fn', ()=>{
	const req = {
		method: 'GET',
		url: '/!',
		headers:{}
	};

	const r = new Request(req).redirect({ method: 'GET' });

	const res = {
		setHeader: jest.fn()
	}

	r(res);

	expect(res.statusCode).toBe(303);
	expect(res.setHeader).toHaveBeenCalledWith('Location', '/');
})

// test('Get delete url', () => {
// 	const r = new Request({ url: '/', method: 'GET' });

// 	expect(r.delete('/a')).toEqual('/a!!');
// 	expect(r.delete()).toEqual('/!!');

// 	expect(new Request({ url: '/!!', method: 'POST' }).method).toBe('DELETE');
// })

test('Get headers', () => {
	const req = {
		url: '/.json',
		headers: {
			a: 'b'
		}
	};

	expect(new Request(req).headers).toMatchSnapshot();
})