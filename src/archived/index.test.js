import archived from './index.js';

test('Archived', async ()=>{
	const req = {
		headers: {}
	};
	const res = {
		writeHead(){},
		end(){}
	};
	expect(await archived(req, res)).toBe(true);
})