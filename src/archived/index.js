import path from 'path';
import serve from 'serve-handler';

const dir = path.join(new URL(import.meta.url).pathname, '..', 'src');

export default async (req, res) => {
	return (await serve(req, res, {
		public: dir,
		directoryListing: false,
		etag: true
	},{
		sendError(/*...args*/){
			return false;
		}
	})) ?? true;
};
