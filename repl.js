import repl from 'repl';
import { ping, server } from './dev.js';

console.log('Ping available on globals.');

const r = repl.start('> ');

r.context.ping = ping;

r.on('exit', ()=> { server.close(()=>console.log('Server disconnected')); });
