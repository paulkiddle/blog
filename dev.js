import start from 'simple-wsgi';
import knex from 'knex-sqlite';
import blog, { webmentionEndpoint, worker } from './src/index.js';
import { fetch, Ping } from './dev/webmention/main.js';
import Queue from 'memory-message-queue';
import IteratorWorker from 'iterator-worker';
import keypair from 'keypair'

function notify(subject, body) {
	console.log(`--- Send notification --
subject: ${subject}

${body}
`)
}

const port = process.env.PORT || 8080;
const origin = process.env.ORIGIN || 'http://localhost:' + port;

const sql = knex('dev/db.sqlite');
const queue = new Queue(0)

const app = await blog({
	origin,
	sql,
	jwtSecret: 'JWT_SECRET',
	notify,
	clientName: 'mrkiddle.co.uk (dev)',
	enqueue: ({ source, target })=>queue.send({ source, target }),
	keypair: keypair()
});

const iw = IteratorWorker.start(
	worker({ sql, notify, queue, fetch, log: e=>console.error(e) })
);

const server = await start(app, port);
server.on('close', () => {
	sql.destroy(()=>console.log('SQL Connection closed'));
	worker.return();
	iw.join().then(()=>console.log('Worker process stopped'));
})

const ping = Ping(origin, webmentionEndpoint);

export { ping, server };
