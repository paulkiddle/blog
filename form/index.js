import html from 'encode-html-template-tag';

const getBody = req => new Promise((resolve, reject) => {
	let body = '';
	req.on('data', chunk => body += chunk);
	req.on('end', () => resolve(body));
	req.on('error', reject);
});

const attrs = params => params ? Object.entries(params).map(([name, value])=>html` ${name}="${value}"`) : '';
const element = name => (params, children) => html`<${name}${attrs(params)}>${children}</${name}>`;

const form = (getComponent, parse) => {
	const form = element('form');
	const component = getComponent(form);
	component.parse = parse;
	return component;
}

const postForm = (getComponent, parse) => form(
	form => {
		const postForm = (params, children) => form({ method: 'POST', ...params }, children);
		return getComponent(postForm)
	},
	async req => parse(new URLSearchParams(await getBody(req)))
);

const getMultipartBody = (req, fileSaver) => {
	const busboy = new Busboy({ headers: req.headers });
	const data = {};
	const save = (field, value) => {
		(data[field] = data[field] || []).push(value);
	}

	busboy.on('field', save);
	busboy.on('file', async(field, ...args) => {
		save(field, await fileSaver(field, ...args));
	});

	req.pipe(busboy);

	return new Promise(resolve => {
		busboy.on('finish', resolve({
			get: key => data[key] && data[key][0],
			getAll: key => data[key] || []
		}));
	});
}

const multipartForm = (params, children, saveFile, parse) => form(
	html`<form method="POST" enctype='multipart/form-data'${attrs(params)}>${children}</form>`,
	async req => parse(await getMultipartBody(req, saveFile))
);

const getGetBody = req => new URLSearchParams(req.url.split('?')[1]);


const withCsrf = (name, generateToken, verifyToken) => formFn => (component, parse) => formFn(
	html`<input type="hidden" name="${name}" value="${generateToken()}">
	${component}`,
	body => {
		if(verifyToken(body.get(name))) {
			return parse(body);
		} else {
			throw new TypeError('CSRF Token')
		}
	}
)

const getForm = (params, children, parse) => form(
	html`<form method='GET'${attrs(params)}>${children}</form>`,
	req => parse(getGetBody(req))
);

export default postForm;
